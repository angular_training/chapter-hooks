export class Task {
  id: number;
  title: string;
  hide: boolean;

  constructor(id: number, title: string) {
    this.id = id;
    this.title = title;
    this.hide = true;
  }

  toggle() {
    this.hide = !this.hide;
  }

}
