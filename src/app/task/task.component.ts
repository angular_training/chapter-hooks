import { Component, Input, SimpleChanges, 
  OnChanges,
  OnInit,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy } from '@angular/core';
import { Task } from '../model/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnChanges,
OnInit,
DoCheck,
AfterContentInit,
AfterContentChecked,
AfterViewInit,
AfterViewChecked,
OnDestroy {

  @Input('task') data: Task;
  
  constructor() {
    console.log(`new - data is ${JSON.stringify(this.data)}`);
  }

  // ngOnChanges() {
  //   console.log(`ngOnChanges - data is ${this.data}`);
  // }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`ngOnChanges - data is ${JSON.stringify(this.data)}`);
    for (let key in changes) {
      console.log(`${key} changed. Current: ${changes[key].currentValue}. Previous: ${changes[key].previousValue}`);
    }
  }

  ngOnInit() {
    console.log(`ngOnInit  - data is ${JSON.stringify(this.data)}`);
  }

  ngDoCheck() {
    console.log("ngDoCheck")
  }

  ngAfterContentInit() {
    console.log("ngAfterContentInit");
  }

  ngAfterContentChecked() {
    console.log("ngAfterContentChecked");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy() {
    console.log("ngOnDestroy");
  }

}
