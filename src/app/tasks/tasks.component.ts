import { Component } from '@angular/core';
import { Task } from '../model/task.model';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent {

  tasks: Task[] = [];

  addTask() {
    const id = Math.floor(Math.random() * 10);
    this.tasks.unshift(new Task(id, `new task (${id})`));
  }

  deleteTask() {
    this.tasks = []
  }

}
